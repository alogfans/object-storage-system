#!/bin/bash
export GOPATH="`pwd`/.."
go build moss/server
go build moss/example
go build moss/prepare-util

./prepare-util

echo "To continue, please modify /etc/resolv.conf to 127.0.0.1"
echo "and insert items with wildcard [*] to /etc/hosts!"

# sudo python dnsproxy.py -s 8.8.8.8
