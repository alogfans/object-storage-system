package restful

import (
	"testing"
	"github.com/gorilla/mux"
	"net/http"
	"log"
	"net/http/httputil"
)

func TestMUX(t *testing.T) {
	r := mux.NewRouter()

	r.Path("/{bucket}/{object:.+}").Queries("acl", "").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.Println("xxx")
		vars := mux.Vars(request)
		log.Println(vars)
	})

	r.Path("/{bucket}/{object:.+}").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		log.Println(vars)
	})

	r.PathPrefix("/").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		b, _ := httputil.DumpRequest(request, false)
		log.Println(string(b))
	})

	http.Handle("/", r)
	err := http.ListenAndServe(":10086", nil)
	if err != nil {
		log.Println(err)
	}
}