package meta

import "github.com/jinzhu/gorm"

var dbHook *gorm.DB

func Bind(connString string) error {
	var err error
	dbHook, err = gorm.Open("sqlite3", connString)
	return err
}

func Close() error {
	return dbHook.Close()
}
