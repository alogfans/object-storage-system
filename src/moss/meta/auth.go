package meta

import (
	_ "github.com/mattn/go-sqlite3"
	// "moss/conf"
)

type Auth struct {
	AccessKeyId     string `gorm:"primary_key"`
	AccessKeySecret string `gorm:"not null"`
	AuthID          string `gorm:"not null"`
	DisplayName     string `gorm:"not null"`
	Status          string `gorm:"not null"`
}

func InitAuthTable() {
	if !dbHook.HasTable(&Auth{}) {
		dbHook.CreateTable(&Auth{})
	}

/*
	_, ok := GetAuthInfo(conf.Config.Admin.AccessKeyId)
	if !ok {
		dbHook.Create(&Auth{
			AccessKeyId:     conf.Config.Admin.AccessKeyId,
			AccessKeySecret: conf.Config.Admin.AccessKeySecret,
			AuthID:          conf.Config.Admin.AuthId,
			DisplayName:     conf.Config.Admin.DisplayName,
			Status:          conf.AccessKeyActive})
	}
*/
}

func CreateDatabaseTables() {
	InitAuthTable()
	InitBucket()
	InitObjectInfo()
	InitMultipartUpload()
	InitUploadPartTable()
	InitRefererList()
	InitLogging()
	InitWebsite()
	InitLifeCycle()
}

func GetAuthInfo(accessKeyId string) (*Auth, bool) {
	var authEntry Auth
	resp := dbHook.First(&authEntry, "access_key_id = ?", accessKeyId)
	if resp.RecordNotFound() {
		return nil, false
	}
	return &authEntry, true
}

func GetAccessKeySecret(accessKeyId string) (string, bool) {
	table, ok := GetAuthInfo(accessKeyId)
	if !ok {
		return "", false
	}
	return table.AccessKeySecret, true
}
