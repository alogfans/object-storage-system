package status

import (
	"net/http"
)

type Status int

type StatusDescription struct {
	ErrorId        int
	Code           string
	Message        string
	HttpStatusCode int
}

const (
	Success                             = 0
	AccessDenied                        = 1
	BucketAlreadyExists                 = 2
	BucketNotEmpty                      = 3
	EntityTooLarge                      = 4
	EntityTooSmall                      = 5
	FieldItemTooLong                    = 6
	FilePartInterity                    = 7
	FilePartNotExist                    = 8
	FilePartStale                       = 9
	IncorrectNumberOfFilesInPOSTRequest = 10
	InvalidArgument                     = 11
	InvalidAccessKeyId                  = 12
	InvalidBucketName                   = 13
	InvalidDigest                       = 14
	InvalidEncryptionAlgorithmError     = 15
	InvalidObjectName                   = 16
	InvalidPart                         = 17
	InvalidPartOrder                    = 18
	InvalidPolicyDocument               = 19
	InvalidTargetBucketForLogging       = 20
	InternalError                       = 21
	MalformedXML                        = 22
	MalformedPOSTRequest                = 23
	MaxPOSTPreDataLengthExceededError   = 24
	MethodNotAllowed                    = 25
	MissingArgument                     = 26
	MissingContentLength                = 27
	NoSuchBucket                        = 28
	NoSuchKey                           = 29
	NoSuchUpload                        = 30
	NotImplemented                      = 31
	PreconditionFailed                  = 32
	RequestTimeTooSkewed                = 33
	RequestTimeout                      = 34
	RequestIsNotMultiPartContent        = 35
	SignatureDoesNotMatch               = 36
	TooManyBuckets                      = 37
	NotModified                         = 38
	InvalidLocationConstraint           = 39
	SymlinkTargetNotExist               = 40
	InvalidTargetType                   = 41
	ObjectNotAppendable                 = 42
	PositionNotEqualToLength            = 43
	CallbackFailed                      = 44
	NoSuchWebsiteConfiguration          = 45
	NoSuchLifeCycle                     = 46
	NoLogging                           = 47
)

var ossErrorInfo = []StatusDescription{
	{Success, "Success", "Success", 200},
	{AccessDenied, "AccessDenied", "AccessDenied", 403},
	{BucketAlreadyExists, "BucketAlreadyExists", "BucketAlreadyExists", 409},
	{BucketNotEmpty, "BucketNotEmpty", "BucketNotEmpty", 409},
	{EntityTooLarge, "EntityTooLarge", "EntityTooLarge", 400},
	{EntityTooSmall, "EntityTooSmall", "EntityTooSmall", 400},
	{FieldItemTooLong, "FieldItemTooLong", "FieldItemTooLong", 400},
	{FilePartInterity, "FilePartInterity", "FilePartInterity", 400},
	{FilePartNotExist, "FilePartNotExist", "FilePartNotExist", 400},
	{FilePartStale, "FilePartStale", "FilePartStale", 400},
	{IncorrectNumberOfFilesInPOSTRequest, "IncorrectNumberOfFilesInPOSTRequest", "IncorrectNumberOfFilesInPOSTRequest", 400},
	{InvalidArgument, "InvalidArgument", "InvalidArgument", 400},
	{InvalidAccessKeyId, "InvalidAccessKeyId", "InvalidAccessKeyId", 403},
	{InvalidBucketName, "InvalidBucketName", "InvalidBucketName", 400},
	{InvalidDigest, "InvalidDigest", "InvalidDigest", 400},
	{InvalidEncryptionAlgorithmError, "InvalidEncryptionAlgorithmError", "InvalidEncryptionAlgorithmError", 400},
	{InvalidObjectName, "InvalidObjectName", "InvalidObjectName", 400},
	{InvalidPart, "InvalidPart", "InvalidPart", 400},
	{InvalidPartOrder, "InvalidPartOrder", "InvalidPartOrder", 400},
	{InvalidPolicyDocument, "InvalidPolicyDocument", "InvalidPolicyDocument", 400},
	{InvalidTargetBucketForLogging, "InvalidTargetBucketForLogging", "InvalidTargetBucketForLogging", 400},
	{InternalError, "InternalError", "InternalError", 500},
	{MalformedXML, "MalformedXML", "MalformedXML", 400},
	{MalformedPOSTRequest, "MalformedPOSTRequest", "MalformedPOSTRequest", 400},
	{MaxPOSTPreDataLengthExceededError, "MaxPOSTPreDataLengthExceededError", "MaxPOSTPreDataLengthExceededError", 400},
	{MethodNotAllowed, "MethodNotAllowed", "MethodNotAllowed", 405},
	{MissingArgument, "MissingArgument", "MissingArgument", 411},
	{MissingContentLength, "MissingContentLength", "MissingContentLength", 411},
	{NoSuchBucket, "NoSuchBucket", "NoSuchBucket", 404},
	{NoSuchKey, "NoSuchKey", "NoSuchKey", 404},
	{NoSuchUpload, "NoSuchUpload", "NoSuchUpload", 404},
	{NotImplemented, "NotImplemented", "NotImplemented", 400},
	{PreconditionFailed, "PreconditionFailed", "PreconditionFailed", 412},
	{RequestTimeTooSkewed, "RequestTimeTooSkewed", "RequestTimeTooSkewed", 403},
	{RequestTimeout, "RequestTimeout", "RequestTimeout", 400},
	{RequestIsNotMultiPartContent, "RequestIsNotMultiPartContent", "RequestIsNotMultiPartContent", 400},
	{SignatureDoesNotMatch, "SignatureDoesNotMatch", "SignatureDoesNotMatch", 403},
	{TooManyBuckets, "TooManyBuckets", "TooManyBuckets", 400},
	{NotModified, "NotModified", "NotModified", 304},
	{InvalidLocationConstraint, "InvalidLocationConstraint", "InvalidLocationConstraint", 400},
	{SymlinkTargetNotExist, "SymlinkTargetNotExist", "SymlinkTargetNotExist", 404},
	{InvalidTargetType, "InvalidTargetType", "InvalidTargetType", 400},
	{ObjectNotAppendable, "ObjectNotAppendable", "ObjectNotAppendable", 409},
	{PositionNotEqualToLength, "PositionNotEqualToLength", "PositionNotEqualToLength", 409},
	{CallbackFailed, "CallbackFailed", "CallbackFailed", 203},
	{NoSuchWebsiteConfiguration, "NoSuchWebsiteConfiguration", "NoSuchWebsiteConfiguration", 404},
	{NoSuchLifeCycle, "NoSuchLifeCycleConfiguration", "NoSuchLifeCycleConfiguration", 404},
	{NoLogging, "NoLogging", "NoLogging", 404},
}

func GetHttpStatusCode(status Status) int {
	for _, v := range ossErrorInfo {
		if v.ErrorId == int(status) {
			return v.HttpStatusCode
		}
	}
	return http.StatusBadRequest
}

func GetCodeString(status Status) string {
	for _, v := range ossErrorInfo {
		if v.ErrorId == int(status) {
			return v.Code
		}
	}
	return ""
}

func GetMessage(status Status) string {
	for _, v := range ossErrorInfo {
		if v.ErrorId == int(status) {
			return v.Message
		}
	}
	return ""
}
