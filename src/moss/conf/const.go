package conf

const (
	PermissionBucketDefault   = "public-read-write"
	PermissionObjectDefault   = "default"
	PermissionPublicReadWrite = "public-read-write"
	PermissionPublicRead      = "public-read"
	PermissionPrivate         = "private"
)

const (
	AccessKeyActive  = "AccessKeyActive"
	AccessKeyRevoked = "AccessKeyRevoked"
)

const AuthorizationPrefix = "OSS "

const (
	HTTPHeaderAcceptEncoding                 = "Accept-Encoding"
	HTTPHeaderAuthorization                  = "Authorization"
	HTTPHeaderCacheControl                   = "Cache-Control"
	HTTPHeaderContentDisposition             = "Content-Disposition"
	HTTPHeaderContentEncoding                = "Content-Encoding"
	HTTPHeaderContentLength                  = "Content-Length"
	HTTPHeaderContentMD5                     = "Content-MD5"
	HTTPHeaderContentType                    = "Content-Type"
	HTTPHeaderContentLanguage                = "Content-Language"
	HTTPHeaderContentRange                   = "Content-Range"
	HTTPHeaderDate                           = "X-Amz-Date"
	HTTPHeaderEtag                           = "ETag"
	HTTPHeaderExpires                        = "Expires"
	HTTPHeaderHost                           = "Host"
	HTTPHeaderLastModified                   = "Last-Modified"
	HTTPHeaderRange                          = "Range"
	HTTPHeaderLocation                       = "Location"
	HTTPHeaderOrigin                         = "Origin"
	HTTPHeaderServer                         = "Server"
	HTTPHeaderUserAgent                      = "User-Agent"
	HTTPHeaderIfModifiedSince                = "If-Modified-Since"
	HTTPHeaderIfUnmodifiedSince              = "If-Unmodified-Since"
	HTTPHeaderIfMatch                        = "If-Match"
	HTTPHeaderIfNoneMatch                    = "If-None-Match"
	HTTPHeaderOssACL                         = "X-Amz-Acl"
	HTTPHeaderOssMetaPrefix                  = "X-Amz-Meta-"
	HTTPHeaderOssObjectACL                   = "X-Amz-Object-Acl"
	HTTPHeaderOssSecurityToken               = "X-Amz-Security-Token"
	HTTPHeaderOssServerSideEncryption        = "X-Amz-Server-Side-Encryption"
	HTTPHeaderOssCopySource                  = "X-Amz-Copy-Source"
	HTTPHeaderOssCopySourceRange             = "X-Amz-Copy-Source-Range"
	HTTPHeaderOssCopySourceIfMatch           = "X-Amz-Copy-Source-If-Match"
	HTTPHeaderOssCopySourceIfNoneMatch       = "X-Amz-Copy-Source-If-None-Match"
	HTTPHeaderOssCopySourceIfModifiedSince   = "X-Amz-Copy-Source-If-Modified-Since"
	HTTPHeaderOssCopySourceIfUnmodifiedSince = "X-Amz-Copy-Source-If-Unmodified-Since"
	HTTPHeaderOssMetadataDirective           = "X-Amz-Metadata-Directive"
	HTTPHeaderOssNextAppendPosition          = "X-Amz-Next-Append-Position"
	HTTPHeaderOssRequestID                   = "X-Amz-Request-Id"
	HTTPHeaderOssCRC64                       = "X-Amz-Hash-Crc64ecma"
	HTTPHeaderOssSymlinkTarget               = "X-Amz-Symlink-Target"

	HTTPHeaderOssObjectType   = "x-Amz-object-type"
	HTTPHeaderOssStorageClass = "x-Amz-storage-class"

	HTTPHeaderResponseContentType        = ""
	HTTPHeaderResponseContentLanguage    = ""
	HTTPHeaderResponseExpires            = ""
	HTTPHeaderResponseCacheControl       = ""
	HTTPHeaderResponseContentDisposition = ""
	HTTPHeaderResponseContentEncoding    = ""

	HTTPHeaderOssCallback    = "x-Amz-callback"
	HTTPHeaderOssCallbackVar = "x-Amz-callback-var"
)

const (
	MIMEIdentifierXML = "application/xml"
)

const (
	ObjectTypeNormal           = "Normal"
	ObjectTypeSymlink          = "Symlink"
	ObjectTypeAppendable       = "Appendable"
	ObjectStorageClassStandard = "Standard"
)

const (
	AccessTypeRead  = "Read"
	AccessTypeWrite = "Write"
)

const (
	ParameterMaxLength           = 512
	HttpMultiDeleteBodyMaxLength = 2 * 1024 * 1024
	MaxDeleteMultiObjects        = 1000
	MaxMemory                    = 5368709120

	MaxCallbackLength    = 5 * 1024
	MaxCallbackVarLength = 5 * 1024
	MaxCallbackUrlNumber = 5
)

const (
	UserMetaMaxSize = 8 * 1024
	UserMeaSep      = ","
)

const (
	OssMetadataDirectiveCopy    = "COPY"
	OssMetadataDirectiveReplace = "REPLACE"
)

const (
	PolicyConditionsbucket                = "bucket"
	PolicyConditionskey                   = "key"
	PolicyConditionsSuccessActionRedirect = "success_action_redirect"
	PolicyConditionsSuccessActionStatus   = "success_action_status"
	PolicyConditionsEq                    = "eq"
	PolicyConditionsStartsWith            = "starts-with"
	PolicyConditionsContentLengthRange    = "content-length-range"

	OssFormFieldPolicy                  = "policy"
	OssFormFieldKey                     = "key"
	OssFormFieldKeyValueSuffix          = "${filename}"
	OssFormFieldOSSAccessKeyId          = "AWSAccessKeyId"
	OssFormFieldSignature               = "Signature"
	OssFormFieldOssServerSideEncryption = "x-Amz-server-side-encryption"
	OssFormFieldOssMetaPrefix           = "x-Amz-meta-"

	OssFormFieldCallback = "callback"
)

const OssTimeFormat = "2006-01-02T15:04:05Z"
