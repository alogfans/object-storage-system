package conf

var Config = struct {
/*
	Admin struct {
		AccessKeyId     string
		AccessKeySecret string
		AuthId          string
		DisplayName     string
	}
*/

	Path struct {
		MetaFile string
		Data     string
		Temp     string
	}

	Name struct {
		DataCenterList     string `default:"us-east-1"`
		ServiceHostPattern string `default:"s3.amazonaws.com"`
		BucketHostPattern  string `default:"BUCKET.s3.amazonaws.com"`
		ObjectPathPattern  string `default:"/OBJECT"`

		// Note: For those cannot done virtual host routing
		// BucketHostPattern: s3.amazonaws.com
		// ObjectPathPattern: /{bucket}/{object}
	}

	Limit struct {
		MaxBucketCount         int   `default:"10"`
		RequestTimeSkewLimit   int64 `default:"900"`
		DefaultMaxKeys         int   `default:"100"`
		FileTransferBufferSize int64 `default:"1048576"`
		MaxTransferSeconds     int   `default:"30"`
		MaxFileSize            int64 `default:"5368709120"`
		MaxCopyObjectSize      int64 `default:"1073741824"`
		MaxObjectKeyLength     int   `default:"1023"`
		DefaultMaxParts        int   `default:"1000"`
	}

	Security struct {
		AesPrivateKey string `default:"0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"`
	}
}{}
