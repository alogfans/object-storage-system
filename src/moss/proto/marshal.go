package proto

import (
	"encoding/xml"
)

type CreateBucketConfiguration struct {
	XMLName            xml.Name `xml:"CreateBucketConfiguration"`
	LocationConstraint string   `xml:"LocationConstraint"`
	StorageClass       string   `xml:"StorageClass"`
}

type LocationConstraint struct {
	XMLName  xml.Name `xml:"LocationConstraint"`
	Xmlns    string   `xml:"xmlns,attr"`
	Location string   `xml:"Location"`
}

type Error struct {
	XMLName       xml.Name `xml:"Error"`
	Code          string   `xml:"Code"`
	Message       string   `xml:"Message"`
	RequestId     string   `xml:"RequestId"`
	HostId        string   `xml:"HostId"`
	Method        string   `xml:"Method"` // For MethodNotAllowed
	ResourceType  string   `xml:"ResourceType"`
	Header        string   `xml:"Header"` // For NotImplemented
	BucketName    string   `xml:"BucketName"`
	ArgumentName  string   `xml:"ArgumentName"`
	ArgumentValue string   `xml:"ArgumentValue"`
}

type ListBucketResult struct {
	XMLName        xml.Name       `xml:"ListBucketResult"`
	Name           string         `xml:"Name"`
	Prefix         string         `xml:"Prefix"`
	KeyCount       int            `xml:"KeyCount"`
	Marker         string         `xml:"Marker"`
	NextMarker     string         `xml:"NextMarker"`
	MaxKeys        int            `xml:"MaxKeys"`
	Delimiter      string         `xml:"Delimiter"`
	IsTruncated    bool           `xml:"IsTruncated"`
	Contents       []Contents     `xml:"Contents"`
	CommonPrefixes CommonPrefixes `xml:"CommonPrefixes"`
}

type Contents struct {
	XMLName      xml.Name `xml:"Contents"`
	Key          string   `xml:"Key"`
	LastModified string   `xml:"LastModified"`
	ETag         string   `xml:"ETag"`
	Type         string   `xml:"Type"`
	Size         int64    `xml:"Size"`
	StorageClass string   `xml:"StorageClass"`
	Owner        Owner    `xml:"Owner"`
}

type Owner struct {
	XMLName     xml.Name `xml:"Owner"`
	ID          string   `xml:"ID"`
	DisplayName string   `xml:"DisplayName"`
}

type CommonPrefixes struct {
	XMLName xml.Name `xml:"CommonPrefixes"`
	Prefix  []string `xml:"Prefix"`
}

type ListAllMyBucketsResult struct {
	XMLName     xml.Name        `xml:"ListAllMyBucketsResult"`
	Owner       Owner           `xml:"Owner"`
	Buckets     BucketContainer `xml:"Buckets"`
	Prefix      string          `xml:"Prefix"`
	Marker      string          `xml:"Marker"`
	NextMarker  string          `xml:"NextMarker"`
	MaxKeys     int             `xml:"MaxKeys"`
	IsTruncated bool            `xml:"IsTruncated"`
}

type BucketContainer struct {
	XMLName xml.Name `xml:"Buckets"`
	Bucket  []Bucket `xml:"Bucket"`
}

type Bucket struct {
	XMLName           xml.Name          `xml:"Bucket"`
	CreationDate      string            `xml:"CreationDate"`
	ExtranetEndpoint  string            `xml:"ExtranetEndpoint"`
	IntranetEndpoint  string            `xml:"IntranetEndpoint"`
	Location          string            `xml:"Location"`
	Name              string            `xml:"Name"`
	Owner             Owner             `xml:"Owner"`
	AccessControlList AccessControlList `xml:"AccessControlList"`
}

type BucketLoggingStatus struct {
	XMLName        xml.Name       `xml:"BucketLoggingStatus"`
	LoggingEnabled LoggingEnabled `xml:"LoggingEnabled"`
}

type LoggingEnabled struct {
	XMLName      xml.Name `xml:"LoggingEnabled"`
	TargetBucket string   `xml:"TargetBucket"`
	TargetPrefix string   `xml:"TargetPrefix"`
}

type WebsiteConfiguration struct {
	XMLName       xml.Name      `xml:"WebsiteConfiguration"`
	Xmlns         string        `xml:"xmlns,attr"`
	IndexDocument IndexDocument `xml:"IndexDocument"`
	ErrorDocument ErrorDocument `xml:"ErrorDocument"`
}

type IndexDocument struct {
	XMLName xml.Name `xml:"IndexDocument"`
	Suffix  string   `xml:"Suffix"`
}

type ErrorDocument struct {
	XMLName xml.Name `xml:"ErrorDocument"`
	Key     string   `xml:"Key"`
}

type RefererConfiguration struct {
	XMLName           xml.Name    `xml:"RefererConfiguration"`
	AllowEmptyReferer bool        `xml:"AllowEmptyReferer"`
	RefererList       RefererList `xml:"RefererList"`
}

type RefererList struct {
	XMLName xml.Name `xml:"RefererList"`
	Referer []string `xml:"Referer"`
}

type LifecycleConfiguration struct {
	XMLName xml.Name `xml:"LifecycleConfiguration"`
	Rule    []Rule   `xml:"Rule"`
}

type Rule struct {
	XMLName              xml.Name             `xml:"Rule"`
	ID                   string               `xml:"ID"`
	Prefix               string               `xml:"Prefix"`
	Status               string               `xml:"Status"`
	Expiration           Expiration           `xml:"Expiration"`
	AbortMultipartUpload AbortMultipartUpload `xml:"AbortMultipartUpload"`
}

type Expiration struct {
	XMLName           xml.Name `xml:"Expiration"`
	Days              uint     `xml:"Days"`
	CreatedBeforeDate string   `xml:"CreatedBeforeDate"`
}

type AbortMultipartUpload struct {
	XMLName           xml.Name `xml:"AbortMultipartUpload"`
	Days              uint     `xml:"Days"`
	CreatedBeforeDate string   `xml:"CreatedBeforeDate"`
}

type AccessControlPolicy struct {
	XMLName           xml.Name          `xml:"AccessControlPolicy"`
	Owner             Owner             `xml:"Owner"`
	AccessControlList AccessControlList `xml:"AccessControlList"`
}

type AccessControlList struct {
	XMLName xml.Name `xml:"AccessControlList"`
	Grant   string   `xml:"Grant"`
}

type BucketInfo struct {
	XMLName xml.Name `xml:"BucketInfo"`
	Bucket  Bucket   `xml:"Bucket"`
}

type CopyObjectResult struct {
	XMLName      xml.Name `xml:"CopyObjectResult"`
	LastModified string   `xml:"LastModified"`
	ETag         string   `xml:"ETag"`
}

type Delete struct {
	XMLName xml.Name `xml:"Delete"`
	Quiet   string   `xml:"Quiet"`
	Object  []Object `xml:"Object"`
}

type Object struct {
	XMLName xml.Name `xml:"Object"`
	Key     string   `xml:"Key"`
}

type DeleteResult struct {
	XMLName xml.Name       `xml:"DeleteResult"`
	Deleted []Deleted      `xml:"Deleted"`
	Error   []DeletedError `xml:"Error"`
}

type Deleted struct {
	XMLName xml.Name `xml:"Deleted"`
	Key     string   `xml:"Key"`
}

type DeletedError struct {
	XMLName xml.Name `xml:"Error"`
	Key     string   `xml:"Key"`
	Code    string   `xml:"Code"`
	Message string   `xml:"Message"`
}

type InitiateMultipartUploadResult struct {
	XMLName  xml.Name `xml:"InitiateMultipartUploadResult"`
	Bucket   string   `xml:"Bucket"`
	Key      string   `xml:"Key"`
	UploadId string   `xml:"UploadId"`
}

type CopyPartResult struct {
	XMLName      xml.Name `xml:"CopyPartResult"`
	LastModified string   `xml:"LastModified"`
	ETag         string   `xml:"ETag"`
}

type CompleteMultipartUpload struct {
	XMLName xml.Name `xml:"CompleteMultipartUpload"`
	Part    []Part   `xml:"Part"`
}

type Part struct {
	XMLName      xml.Name `xml:"Part"`
	PartNumber   string   `xml:"PartNumber"`
	ETag         string   `xml:"ETag"`
	LastModified string   `xml:"LastModified"`
	Size         string   `xml:"Size"`
}

type CompleteMultipartUploadResult struct {
	XMLName  xml.Name `xml:"CompleteMultipartUploadResult"`
	Location string   `xml:"Location"`
	Bucket   string   `xml:"Bucket"`
	Key      string   `xml:"Key"`
	ETag     string   `xml:"ETag"`
}

type ListMultipartUploadsResult struct {
	XMLName            xml.Name `xml:"ListMultipartUploadsResult"`
	Bucket             string   `xml:"Bucket"`
	KeyMarker          string   `xml:"KeyMarker"`
	UploadIdMarker     string   `xml:"UploadIdMarker"`
	NextKeyMarker      string   `xml:"NextKeyMarker"`
	NextUploadIdMarker string   `xml:"NextUploadIdMarker"`
	Delimiter          string   `xml:"Delimiter"`
	Prefix             string   `xml:"Prefix"`
	MaxUploads         string   `xml:"MaxUploads"`
	IsTruncated        string   `xml:"IsTruncated"`
	Upload             []Upload `xml:"Upload"`
	CommonPrefixes     []string `xml:"CommonPrefixes>Prefix"`
}

type Upload struct {
	XMLName   xml.Name `xml:"Upload"`
	Key       string   `xml:"Key"`
	UploadId  string   `xml:"UploadId"`
	Initiated string   `xml:"Initiated"`
}

type ListPartsResult struct {
	XMLName              xml.Name `xml:"ListPartsResult"`
	Bucket               string   `xml:"Bucket"`
	Key                  string   `xml:"Key"`
	UploadId             string   `xml:"UploadId"`
	NextPartNumberMarker string   `xml:"NextPartNumberMarker"`
	MaxParts             string   `xml:"MaxParts"`
	IsTruncated          string   `xml:"IsTruncated"`
	Part                 []Part   `xml:"Part"`
}

type CORSConfiguration struct {
	XMLName  xml.Name   `xml:"CORSConfiguration"`
	CORSRule []CORSRule `xml:"CORSRule"`
}

type CORSRule struct {
	XMLName       xml.Name `xml:"CORSRule"`
	AllowedOrigin []string `xml:"AllowedOrigin"`
	AllowedMethod []string `xml:"AllowedMethod"`
	ExposeHeader  []string `xml:"ExposeHeader"`
	AllowedHeader string   `xml:"AllowedHeader"`
	MaxAgeSeconds string   `xml:"MaxAgeSeconds"`
}

type LiveChannelConfiguration struct {
	XMLName     xml.Name `xml:"LiveChannelConfiguration"`
	Description string   `xml:"Description"`
	Status      string   `xml:"Status"`
	Target      Target   `xml:"Target"`
}

type Target struct {
	XMLName      xml.Name `xml:"Target"`
	Type         string   `xml:"Type"`
	FragDuration string   `xml:"FragDuration"`
	FragCount    string   `xml:"FragCount"`
	PlayListName string   `xml:"PlayListName"`
}

type CreateLiveChannelResult struct {
	XMLName     xml.Name    `xml:"CreateLiveChannelResult"`
	PublishUrls PublishUrls `xml:"PublishUrls"`
	PlayUrls    PlayUrls    `xml:"PlayUrls"`
}

type PublishUrls struct {
	XMLName xml.Name `xml:"PublishUrls"`
	Url     string   `xml:"Url"`
}

type PlayUrls struct {
	XMLName xml.Name `xml:"PlayUrls"`
	Url     string   `xml:"Url"`
}

type LiveChannelStat struct {
	XMLName       xml.Name `xml:"LiveChannelStat"`
	Status        string   `xml:"Status"`
	ConnectedTime string   `xml:"ConnectedTime"`
	RemoteAddr    string   `xml:"RemoteAddr"`
	Video         Video    `xml:"Video"`
	Audio         Audio    `xml:"Audio"`
}

type Video struct {
	XMLName   xml.Name `xml:"Video"`
	Width     string   `xml:"Width"`
	Height    string   `xml:"Height"`
	FrameRate string   `xml:"FrameRate"`
	Bandwidth string   `xml:"Bandwidth"`
	Codec     string   `xml:"Codec"`
}

type Audio struct {
	XMLName    xml.Name `xml:"Audio"`
	Bandwidth  string   `xml:"Bandwidth"`
	SampleRate string   `xml:"SampleRate"`
	Codec      string   `xml:"Codec"`
}

type LiveChannelHistory struct {
	XMLName    xml.Name     `xml:"LiveChannelHistory"`
	LiveRecord []LiveRecord `xml:"LiveRecord"`
}

type LiveRecord struct {
	XMLName    xml.Name `xml:"LiveRecord"`
	StartTime  string   `xml:"StartTime"`
	EndTime    string   `xml:"EndTime"`
	RemoteAddr string   `xml:"RemoteAddr"`
}

type ListLiveChannelResult struct {
	XMLName     xml.Name    `xml:"ListLiveChannelResult"`
	Prefix      string      `xml:"Prefix"`
	Marker      string      `xml:"Marker"`
	MaxKeys     string      `xml:"MaxKeys"`
	IsTruncated string      `xml:"IsTruncated"`
	NextMarker  string      `xml:"NextMarker"`
	LiveChannel LiveChannel `xml:"LiveChannel"`
}

type LiveChannel struct {
	XMLName      xml.Name `xml:"LiveChannel"`
	Name         string   `xml:"Name"`
	Description  string   `xml:"Description"`
	LastModified string   `xml:"LastModified"`
}
