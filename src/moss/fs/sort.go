package fs

import (
	"moss/meta"
	"moss/proto"
	"strconv"
)

type PartSort []proto.Part

func (p PartSort) Len() int {
	return len(p)
}
func (p PartSort) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
func (p PartSort) Less(i, j int) bool {
	num1, _ := strconv.ParseInt(p[j].PartNumber, 10, 64)
	num2, _ := strconv.ParseInt(p[i].PartNumber, 10, 64)
	return num1 > num2
}

type MultipartSort []meta.MultipartUpload

func (m MultipartSort) Len() int {
	return len(m)
}

func (m MultipartSort) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func (m MultipartSort) Less(i, j int) bool {
	return m[i].Key > m[j].Key
}
