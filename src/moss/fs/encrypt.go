package fs

import (
	"crypto/aes"
	"crypto/cipher"
	"io"
)

const AES256BlockSize = aes.BlockSize

func EncryptReader(r io.Reader, privateKey []byte) *cipher.StreamReader {
	block, err := aes.NewCipher(privateKey)
	if err != nil {
		return nil
	}
	stream := cipher.NewOFB(block, privateKey[:AES256BlockSize])
	return &cipher.StreamReader{S: stream, R: r}
}

func EncryptWriter(w io.Writer, privateKey []byte) *cipher.StreamWriter {
	block, err := aes.NewCipher(privateKey)
	if err != nil {
		return nil
	}
	stream := cipher.NewOFB(block, privateKey[:AES256BlockSize])
	return &cipher.StreamWriter{S: stream, W: w}
}
