# 云对象存储系统

## 基本信息
* 授权协议：GPL
* 操作系统：Centos
* 开发语言：Go
* 开发厂商：清华大学计算机系
* 项目网站：不适用
* 项目源码下载：
  *  http://113.105.131.176:29780/alogfans/object-storage-system
  *  https://toscode.gitee.com/alogfans/object-storage-system

## 项目描述
云对象存储系统作为云操作系统的重要组成部分，主要负责非结构化数据的储存和管理，用户通过REST API与系统进行交互。云对象存储系统支持服务端加密、访问权限控制、静态网站托管和大对象分段上传等功能。这些API 均与 Amazon S3 和阿里云 OSS 标准兼容，以便于现有的云应用向中国云迁移。

## 技术架构
### 后端
* 文件系统：支持 POSIX 语义的本地文件系统或分布式文件系统
* 元数据管理：基于 SQLite 的数据库
* 配置文件
### 中端
* 方法功能函数
### 前端
* HTTP 服务
* 请求完整性验证
* 请求分发器（Multiplexer）
## 开发环境
* 语言：Golang 1.11
* IDE： JetBrain Goland
* 操作系统：Ubuntu 18.04 LTS 或 Centos 7.5

# 功能模块
## 对象和容器的基本操作
* 创建容器
* 获取容器列表
* 获取容器内对象
* 删除容器
* 上传对象
* 追加对象
* 复制对象
* 下载对象
* 删除对象
* 读取对象头数据

## 对象和容器的高级操作
* 设置或读取容器访问权限
* 设置或读取对象访问权限
* 设置容器为静态网站
* 分段上传对象

# 后台开发环境和依赖
* 语言：Golang 1.11
* IDE： JetBrain Goland
* 操作系统：Ubuntu 18.04 LTS 或 Centos 7.5
* 需要安装 SQLite 运行库

# 项目下载和运行
* 配置 DNS/Hosts 文件，以支持通配符域名（见附件）
* 拉取项目代码
```
git clone https://toscode.gitee.com/alogfans/object-storage-system
cd object-storage-system/bin
```
* 编译
```
./install.sh
```
* 开启服务
```
./server
```
* 在新的命令行窗口执行下面的测试脚本，验证服务工作的正常性
```
./example
```
